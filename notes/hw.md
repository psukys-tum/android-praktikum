# Hardware and Environment

## Data storage

### Shared preferences

Key-value pairs, where values are of **primitive** type.

### Internal storage

Files directly in device's internal storage. By default files are private to the application (other applications cannot access these files)

### External storage

Removable, *world-readable* files.

### Databases

SQLite and other DAO

### Network Connection

Connect to internetz

## Connectivity

...

### WiFi P2P



## Location

## Sensors

## Google Play Services
