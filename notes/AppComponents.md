# Application Components

## Activity

Works by calling several types of callbacks for initialization and destroying.

### onCreate()

> started when system creates the activity

basic logic that sets-up the activity

at the end - transition to **Started** state

### onStart()

> **Started** state - *visible to the user*

final preparations for coming to the foreground and becoming interactive.

at the end - transition to **Resumed** state

### onResume()

> **Resumed** state - activity comes to foreground.

App stays in this state, until something happens to take focus away

In case of interrupt - transition to **Paused** state

### onPause()

> **Paused** state - activity loses focus and is partially visible (for a short time, because for longer time - enters **Stopped** state)

Pause operations that should not continue while the Activity is in **Paused** state.

possible next states: **Stopped** or **Resumed**

### onStop()

> **Stopped** state - no longer visible to the user.

Finalize usage:

- release almost all resources
- perform CPU-internsive operations (save info too databsase, etc.)

possible next states: **Restarting** or **Destroying**

### onRestart()

> Invoked before an activity is restarted (guess the state is still **Restarting**)

Restores teh stateo f the activity from the time it was stopped.

at the end - transition to **Started** state

### onDestroy()

> Invoked before activity an activy is destroyed (guess the state is still **Destroying**)

final event that an activity receives - all the remaining resources are released.

### Saving activity state
using `onSaveInstanceState` and `onRestoreInstanceState` to save some basic information to key-value object outside of activity's scope.
