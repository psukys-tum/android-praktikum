# Introductory lecture

## General information

Weekly meetings until end of January.

Deadlines:

- tests
- lighting talks
- project submission

## Deadlines
End of W2:

- find a group (max 3)
- send the group details to advisor
- prioritize 4 topics

End of W3:

- matched by chair

In W4:

- first group and advisor meeting

End of W5:

- Project requirements and specs

End of W7

- Lighting talk choice

W9:

- Moodle test

W9/10:

- Lighting talks

W15/16:

- final presentation
- documentation
- source

W17:

- report, should probably be filled within the semester.

## Company offers

### Regular Trade

Idea incubator. App for time-place connection platform for people.

The app is ready and probably needs additional features, some finishing touches (face-lift as it is 4 years).

Server side as well.

### Itestra

Mobile car damage assessment. The app assesses the damage.

> Has an idea, but no estimates as vaguely knows the scale.

- damage categorization (not sure whether ml might help this, probably doesn't have the data set)
- microservices to services in backend

### Campus app

- WiFi for indoor navigations/gps
- access/door control for tum facilities. NFC chip integration to unlock doors.
- study cards. Share study cards with fellow students. (sharing study notes)
- interactive chat bot. ~
- improve code quality

### Quartett mobile

An actual mobile app company.

Party Planner. Collaboration tool for parties:

- party manage/sharing
- organize shopping lists, guest list, other docs
- navigation
- carpooling

https://bayern.devfest.de/

### Weptun

App development company.

Maintenance & Repair remote support app.

If a machine, that works 24/7, breaks down, thus AR & video streaming based app that allows the remote expert instruct how to support the machine.

App does video streaming, where expert freezes frame and does annotation on frozen frame. Further extension - AR implementation of marker moving around the machine.

### BMW-Challenge

Mobility budget. App that analyzes user's mobility patterns to give insights on optimizing their mobility and probably providing BMW solutions that are more convenient for the same price.

Tabula Rasa for this problem. Some SDKs give some help on identifying type of transportation without user intervention.
