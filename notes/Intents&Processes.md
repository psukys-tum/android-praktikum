# Intents and Intent Filters

Intent - messaging object used to request an action from another app component.

- name
- action - generic action to perform
- data - data scheme type (i.e. MIME)
- category - grouping information
- extras - kv pairs
- flags - :?

> Always use explicit intent to start own service

## Intent filter

Specific **type of intents** it accepts based on **action**, **data**, and **category**.

> Component *should* declare separate filters for each unique job it can provide.

## Pending intent

Wrapper around **Intent**.

> Grant permission to external application to use contained **Intent** as if it were executed from your app's own process.

# Services

Application component performing long-running opertions.

> No UI

## Types

- Scheduled - cronjob
- Started - started via a `startService()` by another application component
- Bound - bound via a `bindService()` by another application component

Started vs Bound:

- *Started* just fire the service, can't control it later
- *Bound* client-server interaction, where client can as well ask the service to finish.

## Problems

By default service runs in main thread, thus the service itself should create a new thread.

> Use `AsyncTask` or `HandlerThread` instead of `Thread` class

## Content provider

Presents data to external applications as one or more tables (like relational db tables)
