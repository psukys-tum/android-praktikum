# UI Overview

Two main elements:

- **View** - draws something on the screen
- **View Group** - grouping *View* elements

> Keep layout as simple as possible

