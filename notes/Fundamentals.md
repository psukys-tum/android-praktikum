# Android fundamentals

>Course focus on **Android 5 & 6**. There's Android compatibility kit that helps on bringing features to more versions.

## Architecture

- Custom Linux kernel
- Until 6.0 **BusyBox**, after 6.0 **Toybox** cmd line utilities
- Software stack
  - ~JDK 7
  - **Bionic** - std C library
  - Partial X Window System
  - Partial standard GNU libraries.

1. Kernel
2. Hardware Abstraction Layer (HAL) - std interfaces for device hardware
3. Android Runtime (ART) - DVM successor
   - Virtual machines
   - Each Android app executed in separate process (ART instance)
   - JIT and AOT (ahead of time) compilation
   - DEX files - Android bytecode format, optimized for small memory footprint
4. Native C/C++ libs (Android NDK via JNI)
5. Java API
6. Sytems Apps (interaction with other apps)

## Android SDK

SDK includes several packages (cmd tools) for app development:

1. Tools
   - **avdmanager** - android virtual device manager
   - **jobb** - building APK expansion files
   - **lint** - linter
   - **monkeyrunner** - runner for installing and testing package (blackbox clicking?)
   - **sdkmanager** - sdk manager
2. Build tools
   - **apksigner**
   - **zipalign** - optimize APKs

## App components

> essential building blocks of an Android app.

Four types:

1. Activities - reflecting the UI
2. Services - background
3. Broadcast receivers - data exchange abstractions
4. Content providers - abstraction for accessing data from database or somewhere else


### Activities

Entry point for interacting with the user. Representation of a **single screen** with a **user interface**.

Key interactions:

- Keeping track of what user sees
- Caching previous used processes
- Help on handling process destruction
- Support for user-flow implementation


### Services

Generic entry point for keeping an app running in the background

Types:

- **start** - started services tell the system to keep them running until their work is completed.
- **binding** - bound services are run by some other app as it showed an intent of running our service.

### Broadcast receivers

send/receive broadcast messages from the Android system and other apps. Gateway to other components.

### Content providers

Management of shared set of app data (persistent sotrage).

## Component usage

**Activities, services, and broadcast receivers** are activated by an async message (**Intent**)

## Manifest file

- App must declare **all its components** in Manifest file
- Identify **user permissions** the app requires
- Declare **minimum API level**
- Declare **hardware and software features**
- Declare **API libraries** needed to be linked against

## App resources

> images, audio files, relating to visual presentation of the app

each resource has a **unique integer ID**, stored in project's **R class**. Alternative resources for **different device configurations**

## System permissions

Maintaining security by specifying permissions needed by the app. Each application works in a sandbox and needs to explicitly request access to resources and data outside.

Types of permissions:

1. Implicit (automatic) - data/resources outside, that do *little* risk to user or other apps operation.
2. Explicit (grant by user) - data/resources outside, that have high dange to user or other apps operation
